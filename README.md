# TypeORM-Faker

[![pipeline status](https://gitlab.com/typeorm-faker/typeorm-faker/badges/develop/pipeline.svg)](https://gitlab.com/typeorm-faker/typeorm-faker/-/commits/develop)
[![Latest Release](https://gitlab.com/typeorm-faker/typeorm-faker/-/badges/release.svg)](https://gitlab.com/typeorm-faker/typeorm-faker/-/releases)

Generate fake mocks, stubs using your entity settings.

## Install

```
npm install --save-dev typeorm-faker
```

## Usage

```typescript
// import typeorm faker on your test file.
import typeormFaker from 'typeorm-faker';

// here is entity
@Entity('post')
export class Post {
    @PrimaryGeneratedColumn()
    id!: number;

    @Column()
    title!: string;

    @Column()
    text?: string;

    @Column({
        nullable: false,
        default: 0
    })
    likesCount!: number;
}

// generate stubs
const postStubs = typeormFaker.stub(Post);

/**
 * You can specify values for fake entity
 *
 * Post [
 *     { id: 1, title: 'this is test title', text: 'eW2p3tj*A', likesCount: 0 },
 *     { id: 1, title: 'this is test title', text: 'abcdef', likesCount: 0 }
 * ]
 */
const count = 5;
const postStubs = typeormFaker.stub(Post, count, {
    id: 1,
    title: 'this is test title'
});

// or one
/**
 * Post { id: 13326, title: 'pDYFMFO,ZX', text: 'eW2p3tj*A', likesCount: 0 }
 */
const postStub = typeormFaker.stubOne(Post);

/**
 * Post { id: 1, ... }
 */
const postStub = typeormFaker.stubOne(Post, {
    id: 1
});

/**
 * you can generate also typeorm raw one for type inference
 *
 * Post {
 *     post_id: 13326,
 *     post_title: 'pDYFMFO,ZX',
 *     post_text: 'eW2`p3tj*A',
 *     post_likesCount: 0`
 * }
 */
const rawPostStub = typeormFaker.stubRawOne<Post, 'Post', ''>(Post);
const rawPostStub = typeormFaker.stubRawOne<Post, 'Post', ''>(Post, {
    // post_id
    // post_title ...
});

const count = 2;
const rawPostStubArray = typeormFaker.stubRaw<Post, 'Post', ''>(Post, count, {
    post_id: 12345
});
```

### Support Type-Inference for TypeORM Raw Entity

<img src="assets/type-inference-for-typeorm-raws.webp" style="border: 1px solid white">

---

## Use typeormFaker object globally in test

### Global type setting

setting up to tsconfig.json

```json
# set typeRoots on tsconfig.json in compilerOptions
{
    // ...,
    "typeRoots": [
        "node_modules/typeorm-faker/@types"
    ],
    "types": [
        "sample"
    ]
}
```

### Register

Set register file as require settings

```yml
# like .mocharc.yml
- typeorm-faker/registers
```

## Limitations

For type inference of TypeORM raw entity, you must specify class variable and that name, additional fields in test that why
Typescript does not yet support the way that getting class name from generics.

## See Also

### Samples

Here is [Sample Project](https://gitlab.com/typeorm-faker/typeorm-fake-sample).
