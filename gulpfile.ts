import { Gulpclass, MergedTask, SequenceTask, Task } from 'gulpclass';
import * as gulp from 'gulp';
import * as shell from 'gulp-shell';
import * as sourcemaps from 'gulp-sourcemaps';
import * as ts from 'gulp-typescript';

@Gulpclass()
export class Gulpfile {
    @SequenceTask()
    build(): any {
        return ['clean', 'compile', 'copyRegisterFile', 'copyGlobalStaticTypes'];
    }

    @SequenceTask()
    publish(): any {
        return [
            'build',
            'copyPackageJson',
            'copyRegisterFile',
            'copyGlobalStaticTypes',
            'packageCopyReadme',
            'tarball'
        ];
    }

    /**
     * Compile sources with tsc
     */
    @MergedTask()
    compile(): any {
        const tsProject = ts.createProject('tsconfig.json', {
            typescript: require('typescript')
        });
        const tsResult = gulp
            .src(['./src/**/*.ts', '!./src/**/*.spec.ts'])
            .pipe(sourcemaps.init())
            .pipe(tsProject());

        return [
            tsResult.dts.pipe(gulp.dest('./dist')),
            tsResult.js
                .pipe(sourcemaps.write('.', { sourceRoot: '', includeContent: true }))
                .pipe(gulp.dest('./dist'))
        ];
    }

    /**
     * Moves all compiled files to the final package directory.
     */
    @Task()
    copyPackageJson(): any {
        return gulp
            .src('package.json')
            .pipe(gulp.dest('./dist'))
            .pipe(shell('cd dist && npm pkg set scripts.prepare="echo skip husky"'));
    }

    /**
     * Copy registers file
     */
    @Task()
    copyRegisterFile(): any {
        return gulp.src('src/registers.js').pipe(gulp.dest('./dist'));
    }

    /**
     * Copy registers file
     */
    @Task()
    copyGlobalStaticTypes(): any {
        return gulp
            .src('src/@types/typeorm-faker/index.d.ts')
            .pipe(gulp.dest('./dist/@types/typeorm-faker'));
    }

    /**
     * Clean build
     */
    @Task()
    clean(): any {
        return gulp.src('package.json', { read: false }).pipe(shell(['npm run clean']));
    }

    /**
     * Publish with tarball contents
     */
    @Task()
    tarball(): any {
        return gulp.src('package.json', { read: false }).pipe(shell(['cd ./dist && npm publish']));
    }

    /**
     * Copies README.md into the package.
     */
    @Task()
    packageCopyReadme(): any {
        return gulp.src('./README.md').pipe(gulp.dest('./dist'));
    }
}
