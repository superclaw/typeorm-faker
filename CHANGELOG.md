# CHANGELOG

## v1.3.x

-   Support TypeORM QueryBuilder stub
-   Fix global type bug

## v1.2.x

-   Support global type
-   Support namespace for type use

## v1.1.x

-   Support global object import
-   Support stubRaws for generating raw object array

## v1.0.0

-   Support generator for stubs, mocks with custom values
