globalThis.typeormFaker = require('./').default;
globalThis.stub = require('./').default.stub;
globalThis.stubOne = require('./').default.stubOne;
globalThis.stubRaw = require('./').default.stubRaw;
globalThis.stubRawOne = require('./').default.stubRawOne;
