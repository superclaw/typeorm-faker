/* eslint-disable no-unused-expressions */
// eslint-disable-next-line max-classes-per-file
import { describe } from 'mocha';
import {
    Column,
    CreateDateColumn,
    Entity,
    PrimaryGeneratedColumn,
    UpdateDateColumn
} from 'typeorm';
import { ColumnMode } from 'typeorm/metadata-args/types/ColumnMode';
import { EntityPatcher } from './patcher';
import { UtilService } from './util.service';

enum UserType {
    ANONYMOUS = 'anonymous',
    NONCE = 'nonce',
    NORMAL = 'normal',
    ADMIN = 'admin'
}

@Entity()
class User {
    @PrimaryGeneratedColumn()
    id!: number;

    @Column()
    name?: string;

    @Column({
        type: 'varchar'
    })
    nickname?: string;

    @Column({
        nullable: true
    })
    isMale?: boolean | null;

    @Column({
        type: 'enum',
        enum: Object.values(UserType).filter((item) => isNaN(+item)),
        default: UserType.ANONYMOUS
    })
    userType!: UserType;

    @CreateDateColumn()
    createDate!: Date;

    @UpdateDateColumn()
    updateDate!: Date;
}

class SingleSignedOnUser extends User {
    @Column()
    email!: string;

    @Column()
    accessToken!: string;

    signType?: 'google' | 'apple' | null;
}

describe('Test patcher', () => {
    let serviceSandbox: sinon.SinonSandbox;

    after(() => {
        sinon.restore();
    });

    describe('Test Single Entity patch', () => {
        beforeEach(() => {
            serviceSandbox = sinon.createSandbox();
        });

        afterEach(() => {
            serviceSandbox.restore();
        });

        it('should patch entity', () => {
            const stubValue = 'somethingValue';
            serviceSandbox.stub(UtilService, 'isEmptyObject').returns(true);
            serviceSandbox.stub(EntityPatcher, 'patchByMode').returns(stubValue);

            const generatedUserStub = EntityPatcher.patch(User);

            // eslint-disable-next-line no-unused-expressions
            expect(generatedUserStub).ok;
            // eslint-disable-next-line no-unused-expressions
            expect(generatedUserStub instanceof User).ok;
            expect(generatedUserStub.id).equal(stubValue);
            expect(generatedUserStub.userType).equal(stubValue);
        });

        (
            [
                { columnModeMock: 'regular', stubValue: 'somethingValue' },
                { columnModeMock: 'createDate', stubValue: new Date() }
            ] as Array<{ columnModeMock: ColumnMode; stubValue: string | Date }>
        ).forEach(({ columnModeMock, stubValue }) => {
            it(`should patchMode: ${columnModeMock}`, function () {
                serviceSandbox.stub(UtilService, 'isEmptyObject').returns(true);
                serviceSandbox
                    .stub(EntityPatcher, 'patchValueByTypeormColumnOptions')
                    .returns(stubValue);
                serviceSandbox
                    .stub(EntityPatcher, 'patchValueByTypeormColumnMode')
                    .returns(stubValue as Date);

                const generatedUserStub = EntityPatcher.patchByMode(columnModeMock, {
                    type: 'number',
                    default: 0
                });

                // eslint-disable-next-line no-unused-expressions
                expect(generatedUserStub).ok;
                expect(generatedUserStub).equal(stubValue);
            });
        });
    });

    describe('Test Inherited Entity patch', () => {
        it('should patch entity including superclass properties', () => {
            const generatedUserStub = EntityPatcher.patch(SingleSignedOnUser);

            expect(generatedUserStub).ok;
            expect(generatedUserStub instanceof User).ok;
            expect(generatedUserStub instanceof SingleSignedOnUser).ok;
            expect(generatedUserStub.id).ok;
            expect(generatedUserStub.userType).ok;

            expect(generatedUserStub.accessToken).ok;
            expect(generatedUserStub.email).ok;
        });
    });
});
