import { CamelCaseToSnakeCase } from './camel-case-to-snake.type';

/**
 * 아직까지 typescript type 에서 class T 에 대한
 * name 을 추출하는 기능은 존재하지 않는다.
 *
 * CAUTION: ValueOf 는 Union Type 이라 현재 정확한 매칭이 힘들다.
 */
export type TypeORMRawColumns<
    Entity,
    EntityClassName extends string,
    AdditionalFields extends string,
    Joiner = EntityClassName extends '' ? '' : '_',
    ConvertedPropertyAsSnake = `${Uncapitalize<EntityClassName>}${Extract<
        Joiner,
        string
    >}${CamelCaseToSnakeCase<Extract<keyof Entity, string>>}`,
    ValueOf = Entity[keyof Entity]
> = {
    [key in Extract<ConvertedPropertyAsSnake | AdditionalFields, string>]?: ValueOf;
};
